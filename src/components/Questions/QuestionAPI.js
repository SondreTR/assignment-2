export const QuestionAPI = {

    fetchQuestions(questionNumber, category, difficulty, triviaType) { 
        return fetch(`https://opentdb.com/api.php?amount=${questionNumber}&category=${category}&difficulty=${difficulty}&type=${triviaType}`)
            .then(response => response.json())
    },
    
    fetchQuestionCategories() {
        return fetch("https://opentdb.com/api_category.php")
            .then(response => response.json())
    }
}
