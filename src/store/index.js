import Vue from 'vue';
import Vuex from 'vuex';
import { QuestionAPI } from '@/components/Questions/QuestionAPI';

Vue.use(Vuex);

// The place for storing all our states.
export default new Vuex.Store({

    state: {
        questions: [], //all questions
        question: '', //current question
        difficulty: '', //easy, medium, hard
        category: '', 
        categories: '',
        amount: '', //1-50
        type: '', //multiple or boolean
        error: '',
        answers: [], 
        questionNumber: '', //0-49
        userAnswers: [], 
        score: 0, //0-50*10
        round: 0, //0-49
        finished: '',
        correctAnswers: [],
        questionArray: [],
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setQuestion: (state, payload) => {
            state.question = payload;
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload;
        },
        setCategory: (state, payload) => {
            state.category = payload;
        },
        setCategories: (state, payload) => {
            state.categories = payload;
        },
        setAmount: (state, payload) => {
            state.amount = payload;
        },
        setType: (state, payload) => {
            state.type = payload;
        },
        setQuestionNumber: (state, payload) => {
            state.questionNumber = payload;
        },
        setAnswers: (state, payload) => {
            state.answers = payload;
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers = payload;
        },
        setScore: (state, payload) => {
            state.score = payload;
        },
        setRound: (state, payload) => {
            state.round = payload
        },
        setFinished: (state, payload) => {
            state.finished = payload
        },
        setError: (state, payload) => {
            state.error = payload;
        },
        setCorrectAnswers: (state, payload) => {
            state.correctAnswers = payload;
        },
        setQuestionArray: (state, payload) => {
            state.questionArray = payload;
        },
    },
    getters: {},
    actions: {
        
        //fetch categories to present as options
        async fetchQuestionCategories({ commit }) {
            try {
                const categories = await QuestionAPI.fetchQuestionCategories();
                commit('setCategories', categories);
            } catch (e) {
                commit('setError', e.message);
            }
        },

        //use input to fetch questions
        async fetchQuestions({ commit }) {
            try {
                const allQuestions = await QuestionAPI.fetchQuestions(
                    this.state.amount,
                    this.state.category,
                    this.state.difficulty,
                    this.state.type
                )

                for (let i = 0; i < this.state.amount; i++) {
                    commit('setCorrectAnswers', this.state.correctAnswers.concat(allQuestions.results[i].correct_answer));
                    commit('setQuestionArray', this.state.questionArray.concat(allQuestions.results[i].question));
                }
                commit('setQuestions', allQuestions);
                commit('setQuestion', allQuestions.results[this.state.round].question);


                if (this.state.type == 'multiple') {
                    //access the first answerOptions by concatenating wrong answers with correct answers
                    let answerOptions = 
                        allQuestions.results[this.state.round].incorrect_answers.
                            concat(allQuestions.results[this.state.round].correct_answer);        
                    commit('setAnswers', answerOptions.sort(() => 0.5 - Math.random()));
                }
            } catch (e) {
                console.log('Error in fetchquestion process ' + e.message)
                this.error = e.message;
            }
        },

        resetStates({ commit }) {
            commit('setScore', 0);
            commit('setQuestionArray', []);
            commit('setUserAnswers', [])
            commit('setQuestions', [])
            commit('setCorrectAnswers', [])
            commit('setFinished', '')
            commit('setDifficulty', '')
            commit('setType', '')
            commit('setCategory', '')
            commit('setAmount', 1)
            commit('setQuestion', '')
        },


        saveAnswers({ commit }, incomingValue) {
            if (incomingValue == this.state.questions.results[this.state.round].correct_answer) 
                commit('setScore', this.state.score + 10)
            commit('setUserAnswers', this.state.userAnswers.concat(incomingValue))
        },

        nextRound({ commit }) {
            if (this.state.round + 1 == this.state.amount) commit('setFinished', 'finished')
            else {
                commit('setRound', this.state.round + 1)

                if (this.state.type == 'multiple') {
                    let answerOptions = this.state.questions.results[this.state.round].incorrect_answers.
                        concat(this.state.questions.results[this.state.round].correct_answer)    
                    commit('setAnswers', answerOptions.sort(() => 0.5 - Math.random()));
                }
            }
        },
    
        async playAgain({ commit }) {

            commit('setQuestionArray', []);
            commit('setUserAnswers', [])
            commit('setQuestions', [])
            commit('setCorrectAnswers', [])
            commit('setFinished', '')

            try {
                const allQuestions = await QuestionAPI.fetchQuestions(
                    this.state.amount,
                    this.state.category,
                    this.state.difficulty,
                    this.state.type
                )
                commit('setRound', 0)
                for (let i = 0; i < this.state.amount; i++) {
                    commit('setCorrectAnswers', this.state.correctAnswers.concat(allQuestions.results[i].correct_answer));
                    commit('setQuestionArray', this.state.questionArray.concat(allQuestions.results[i].question));
                }

                commit('setQuestions', allQuestions);
                commit('setQuestion', allQuestions.results[this.state.round].question);

                if (this.state.type == 'multiple') {
                    let answerOptions = this.state.questions.results[this.state.round].incorrect_answers.
                        concat(this.state.questions.results[this.state.round].correct_answer)    
                    commit('setAnswers', answerOptions.sort(() => 0.5 - Math.random()));
                }
            } catch (e) {
                console.log('Error in fetchquestion process ' + e.message)
                this.error = e.message;
            }
        },
    },
})
