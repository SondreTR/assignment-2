
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Mainscreen',
        component: () => import(/* webpackChunkName: "start"*/ '../components/Mainscreen/Mainscreen.vue')
    }
    ,
    {
        path: '/trivia',
        name: 'Trivia',
        component: () => import(/* webpackChunkName: "trivia"*/ '../components/Questionscreen/Triviascreen.vue')
    }
    ,
    {
        path: '/results',
        name: 'Results',
        component: () => import(/* webpackChunkName: "results"*/ '../components/Endscreen/Endscreen.vue')
    }
    ]

const router = new VueRouter({ 
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;
